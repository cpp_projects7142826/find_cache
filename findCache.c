#include <emmintrin.h>
#include <x86intrin.h>
#include <stdio.h>
#include <stdint.h>

int main(void){
    char* array;
    char* smallArray;
    char *addr;
    register uint64_t t1, t2;
    long result;
    int size;
    int temp;
    int temp2;
    int junk = 0;

    for(int i = 8; i<26; i++){        
        size = 1<<i;
        array = (char*)malloc(size);
        smallArray = (char*)malloc(1);
        result = 0;
        temp = 0;

        for(int j = 0; j < 40; j++){
            temp2 += smallArray[0];
            
            for(int k = 0; k < size; k++){
                temp += array[k];
            }
            addr = &smallArray[0];
            t1 = __rdtscp(&junk);
            junk = *addr;
            t2 = __rdtscp(&junk) - t1;
            
            result += t2;
        }
        result = result / 10;

        printf("size of data = %d KB, time taken to access = %ld\n", (int)(size * 4 / 1024), result);
    }

}
